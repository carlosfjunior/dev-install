#!/usr/bin/env bash

log=~/dev-install.log
install_folder=/opt/dev-tools
DOCKER_COMPOSE_VERSION="1.15.0"
who=$USER
BUFFERCURSOR=0

main(){
  clear
  local result=1;
  source /etc/os-release

  OS=$ID
  OS_VERSION=$VERSION_ID
  mv $log ~/"dev-install-"$(date +"%m-%d-%y-%H-%M-%m")".log"
  bufferAppend "✔ - OK - Arquivo de log $log criado."
  bufferDisplay
  
  if [[ $ID == "debian" ]];then
    bufferAppend "✔ - OK - Sistema operacional Debian detectado."
    result=0
    _install_git
    _install_docker 
  else
    bufferAppend "❌ - ERROR - Sistema operaciona não homologado!"
    bufferDisplay | grep ERROR
  fi

  if [ $result ];then
    _install 
  else
    bufferAppend "❌ - ERROR - Sistema operaciona não homologado!"
    bufferDisplay  | grep ERROR
    exit 1
  fi
  bufferDisplay | grep OK
  bufferReset 
  exit 0
}

bufferReset (){
    unset BUFFERVAR
    BUFFERCURSOR=0
}
bufferAppend () {
    (( BUFFERCURSOR++ ))
    BUFFERVAR[$BUFFERCURSOR]="$1"
}

bufferDisplay () {
    clear  
    for ((i=1;i<=$BUFFERCURSOR;i++));do 
        printf "%s\n" "${BUFFERVAR[$i]}"; 
    done
}

_install(){
  if check_git_installation;then 
    bufferAppend "❌ - ERROR - GIT Não instalado verificar log  ou tente instalar manualmente"
    bufferDisplay | grep ERROR
    exit 1
  fi
  if [ ! -d  /opt/dev-tools ]; then
    bufferAppend "➜ - INSTALL - Instanlando dev-tools em $install_folder"
    sudo mkdir -p $install_folder
    sudo git clone https://carlosfjunior@bitbucket.org/carlosfjunior/dev-tool.git $install_folder >> $log 2>&1
    _create_bin >> $log 2>&1
    sudo chown $who $install_folder -R >> $log 2>&1

    bufferAppend "➜ - INSTALL - configurando aliases"
    bufferDisplay
    for alias in `ls $install_folder/aliases`;do
      cat $install_folder/aliases/$alias >> ~/.bashrc
    done
    source ~./bashrc
    bufferAppend "✔ - OK - Aliases configurado"

    clear

    if [ ! -d  /opt/dev-tools ]; then
    _abort "Problema na istalação do dev-tools, verifique o $log"
    fi
    bufferAppend "✔ - OK - dev-tool instalado com sucesso"
  else
    bufferAppend "✔ - OK - dev-tool instalado"
  
  fi
}

check(){
  local locate=$1
  [ -d $locate ] || [ -f $locate ] && return 0
}

_remove(){
 if check "/usr/bin/docker"; then
    echo "Removendo o docker"
    sudo docker rm -f $(docker ps -a -q) > /dev/null
    sudo docker rmi -f $(docker images -q) > /dev/null

    for pkg in $( sudo dpkg -l | grep docker | awk '/^[a-z][a-z]/ { print $2 }');
    do
        sudo dpkg -r $pkg > /dev/null
    done
fi


if check "/opt/dev-tools"; then
  sudo rm -rf /opt/dev-tools > /dev/null
  sudo apt-get purge git -y > /dev/null
fi


if check /usr/bin/docker-compose;then
  sudo rm /usr/bin/docker-compose > /dev/null
fi

}

_abort(){
  bufferAppend "❌ - ERROR - $1"
	bufferAppend "❌ - ERROR - Desinstalando ferramenta verifique o arquivo de log $log"
  bufferDisplay
  _remove
  exit 1
}

_create_bin(){
	links=`ls $install_folder/bin`
	for link in $links;do
 		sudo ln -s $install_folder/bin/$link /usr/bin >> $log 2>&1
    check_install bin

	done
}


function _install_docker(){
  if [ ! -f "/usr/bin/docker" ]; then
    bufferAppend "➜ - INSTALL - Instalando docker"
    bufferDisplay
    curl -sSL https://get.docker.com/ | sh >> $log 2>&1
    sudo usermod -aG docker ${USER} >> $log 2>&1
    sudo systemctl enable docker >> $log 2>&1
    if [ ! -f "/usr/bin/docker" ]; then
      _abort "Erro na instalação do docker"  
      bufferDisplay
      _abort
    else
      bufferAppend "✔ - OK - Docker instalado com sucesso."
      bufferDisplay
    fi
  else
    bufferAppend "✔ - OK - Docker instalado"
  fi
  
  if [ ! -f "/usr/bin/docker-compose" ]; then
    bufferAppend "➜ - INSTALL - Instalando o docker-compose"
    bufferDisplay
    curl -sL https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > docker-compose	
    sudo mv docker-compose /usr/bin 
    sudo chmod +x /usr/bin/docker-compose
  fi
}

check_git_installation(){
  if [ -f "/usr/bin/git" ]; then
    return 1
  fi
}

_gitconfig(){
  if [[ ! -f ${HOME}/.gitconfig ]]; then
    git config --global user.name "${FULLNAME}"
    git config --global user.email "${NET_USER}@gmail.com"
  fi

  git config --global include.path ${GIT_PLUGIN_PATH}/config/gitconfig
  git config --global http.sslVerify false
}

_install_git(){

  if check_git_installation; then
    bufferAppend "➜ - INSTALL - Instalando Git"
    bufferDisplay
    sudo apt-get update >> $log 2>&1
    sudo apt-get install git -y  >> $log 2>&1
    if check_git_installation;then
      _abort "Problema na instalação do git"
      exit 1
    else
      bufferAppend "✔ - OK - Git instalado com sucesso."
      bufferDisplay
    fi

  else
    bufferAppend "✔ - OK - Git instalado"
    bufferDisplay
  fi
}


main
