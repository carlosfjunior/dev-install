#!/usr/bin/env bash
clear


check(){
    local locate=$1
    [ -d $locate ] || [ -f $locate ] && return 0
}

bufferReset (){
    unset BUFFERVAR
    BUFFERCURSOR=0
}
bufferAppend () {
    (( BUFFERCURSOR++ ))
    BUFFERVAR[$BUFFERCURSOR]="$1"
}
bufferDisplay () {
    clear  
    for ((i=1;i<=$BUFFERCURSOR;i++));do 
        printf "%s\n" "${BUFFERVAR[$i]}"; 
    done
}

bufferAppend "✔ Desinstalando ferramenta dev-tools e dependencias."

 if check "/usr/bin/docker"; then
    bufferAppend "➜ - REMOVENDO - Docker"
    bufferDisplay
    sudo docker rm -f $(docker ps -a -q) > /dev/null  2>&1
    sudo docker rmi -f $(docker images -q) > /dev/null 2>&1

    for pkg in $( sudo dpkg -l | grep docker | awk '/^[a-z][a-z]/ { print $2 }');
    do
        sudo dpkg -r $pkg > /dev/null 2>&1
    done
fi


if check "/opt/dev-tools"; then
    bufferAppend "➜ - REMOVENDO - dev-tools"
    bufferDisplay
	sudo rm -rf /opt/dev-tools > /dev/null
fi

if check "/usr/bin/git";then
    bufferAppend "➜ - REMOVENDO - git"
    bufferDisplay
    sudo apt-get purge git -y > /dev/null 2>&1
fi


if check /usr/bin/docker-compose;then
    bufferAppend "➜ - REMOVENDO - docker-compose"
    bufferDisplay
	sudo rm /usr/bin/docker-compose > /dev/null 2>&1
fi

bufferAppend "➜ REMOVENDO aliases dev-tools"
sed -i '/dev-tools/d' ~/.bashrc
bufferDisplay
bufferAppend "✔ - Ferramenta removida."
bufferDisplay